package Exception;

public class Basic_Calculator {
    //tip: each method should have your different assign 
    protected double Number1;
    protected double Number2;
    public Basic_Calculator() {
        Number1 = 0.0;
        Number2 = 0.0;
    }
    //Overloading constructor
    public Basic_Calculator(double Number1, double Number2) {
        this.Number1 = Number1;
        this.Number2 = Number2;
    }////without values
    public double Sum() {
        return Number1 + Number2;
    }//with int values
    public double Sum(double Number1, double Number2) {
        return Number1 + Number2;
    }
    public double Subtraction() {
        return Number1 - Number2;
    }
    public double Subtraction(double Number1, double Number2) {
        return Number1 - Number2;
    }
    public double Multiplication() {
        return Number1 * Number2;
    }
    public double Multiplication(double Number1, double Number2) {
        return Number1 * Number2;
    }
    public double Division() {
        return Number1 / Number2;
    }
    public double Division(double Number1, double Number2) {
        return Number1 / Number2;
    }
    public void ExceptionbyZero(double Number1, double Number2) throws ArithmeticException {
        throw new ArithmeticException("Don't Divisable by 0!");
    }
//overloading is 2 or more methods with same name but different assigns
}
