package Exception;

import java.util.Scanner;

public class main {

    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        Basic_Calculator objcalc = new Basic_Calculator();;
        String calcoption = "";
        System.out.println("Welcome, Basic Calculator !");
        System.out.print("Enter first number:");
        double number1 = Double.parseDouble(read.next());
        System.out.print("Enter second number:");
        double number2 = Double.parseDouble(read.next());
        System.out.println("Hello User!, type the operation that are representable by each simbols +, -, * and /");
        calcoption = read.next();
        
        try {
            switch (calcoption) {
                case "+":
                    System.out.println("Sum: " + objcalc.Sum(number1, number2));
                    break;
                case "-":
                    System.out.println("Subtraction: " + objcalc.Subtraction(number1, number2));
                    break;
                case "*":
                    System.out.println("Multiplication: " + objcalc.Multiplication(number1, number2));
                    break;
                case "/":
                    if (number1 == 0) {
                        objcalc.ExceptionbyZero(number1, number2);
                    } else {
                        System.out.println("Division: " + objcalc.Division(number1, number2));
                        break;
                    }
                default:
                    System.out.println("Option Invalid, try again !");
                    break;
            }
        } catch (NumberFormatException a) {
            System.out.println("Please enter a number, try again ! ");
        }
    }
}
